**Desáfio Jack Experts**

1. **O que esperamos**
<ul>
<li>Criar um Chart Helm com as melhores práticas de montagem;</li>
<li>Ter o Chart Helm versionado no repositório GIT</li>
<li>O valor padrãoda imagem deve ser a última imagem "buildada" no repositório Docker</li>
<li>A aplicação deve ter Ingress habilitado por Padrão</li>
<li>A aplicação deve ter HPA habilitado por padrão</li>
</ul>

2. **O que entregar**
<ul>
<li>Repositório GIT contendo Dockerfile, Chart Helm, Aplicação, README explicando o desáfio e como implementar a app</li>
<li>Link público do repositório Docker Hub com a imagem da app</li>
<li>Evidências de implementação em um cluster Kubernets</li>
<li>Evidências da release implementada no Kubernets</li>
</ul>

**No Windows**:

1. **Criação do Chart**:
   Execute o comando abaixo para criar o chart no diretório atual do terminal:
```
helm create nome-do-meu-chart
```

   O comando acima cria um diretório `chart`, contendo arquivos de configuração.

2. **Configuração da Imagem**:
   Dentro do diretório, temos o arquivo `values.yaml`. Configure-o para apontar para o repositório no Docker Hub:
```
image:
 repository: neondev1/my-nginx
 tag: latest
 pullPolicy: IfNotPresent
```

   - `repository`: Refere-se a um repositório disponível no Docker Hub.
   - `tag: latest`: Garante que sempre utilizaremos a última imagem disponível.

3. **Configuração do Ingress**:
   Altere as configurações de ingress no `values.yaml` da seguinte forma:
```
ingress:
 enabled: true
 className: ""
 annotations:
   kubernetes.io/ingress.class: nginx
 hosts:
   - host: app-jooapedroagustinhocosta.helm.jackexperts.com
     paths:
       - path: /
         pathType: ImplementationSpecific
```
   - `enabled: true`: Habilita o ingress por padrão.
   - A `annotations` especifica que o ingress deve ser atendido pelo ingress controller NGINX.
   - O `host` foi modificado para atender às necessidades do desafio. Para utilizar em localhost, altere para "localhost". Após a instalação, use o comando:
```
kubectl port-forward nome_do_pod 8080:80 -n seu_namespace
```
   Este comando encaminha nosso pod para uma porta local.

4. **Configuração do HPA (Horizontal Pod Autoscaling)**:
   Para habilitar o HPA por padrão, ajuste o `values.yaml`:
```
autoscaling:
 enabled: true
 minReplicas: 1
 maxReplicas: 100
 targetCPUUtilizationPercentage: 80
 # targetMemoryUtilizationPercentage: 80
```
   Altere `enabled` para `true` para habilitar o HPA por padrão.

5. **Teste de Implementação**:
   - Crie um namespace:
```
kubectl create namespace seu_namespace
```

   - Instale a aplicação usando o Helm:
     helm install nome_da_release caminho_para_o_diretório_clonado -n seu_namespace

6. **Verificação de Implementação**:
   Para verificar a implementação, além de acessar via localhost, use os comandos:
```
helm list -n seu_namespace
helm history -n seu_namespace nome_da_release
```

7. **Encaminhamento para Localhost**:
   Não se esqueça de encaminhar a porta do pod para o localhost:
```
kubectl port-forward nome_do_pod 8080:80 -n seu_namespace
```

   Agora, basta abrir http://localhost:8080 no seu navegador para ver a aplicação em funcionamento.

**No Linux Debian**:

1. **Criação do Chart com Helm3**:
   No terminal, execute o seguinte comando para criar um novo chart Helm:
```
microk8s.helm3 create nome-do-meu-chart
```

   Esse comando cria um diretório chamado `nome-do-meu-chart` contendo os arquivos de configuração do Helm.

2. **Configuração da Imagem**:
   Dentro do diretório criado, localize o arquivo `values.yaml`. Configure-o para apontar para o seu repositório no Docker Hub:
```
image:
 repository: neondev1/my-nginx
 tag: latest
 pullPolicy: IfNotPresent
```
   - `repository`: Refere-se a um repositório disponível no Docker Hub.
   - `tag: latest`: Garante que sempre utilizaremos a última imagem disponível.

3. **Configuração do Ingress**:
   No mesmo arquivo `values.yaml`, atualize as configurações de ingress conforme abaixo:
```
ingress:
 enabled: true
 className: ""
 annotations:
   kubernetes.io/ingress.class: nginx
 hosts:
   - host: app-jooapedroagustinhocosta.helm.jackexperts.com
     paths:
       - path: /
         pathType: ImplementationSpecific
```
   - `enabled: true`: Habilita o ingress por padrão.
   - `annotations`: Especifica que o ingress deve ser atendido pelo ingress controller NGINX.
   - `host`: Foi modificado conforme o desafio, mas para o localhost, altere para "localhost".

4. **Configuração do HPA (Horizontal Pod Autoscaling)**:
   Continuando no `values.yaml`, para habilitar o HPA por padrão, faça:
```
autoscaling:
 enabled: true
 minReplicas: 1
 maxReplicas: 100
 targetCPUUtilizationPercentage: 80
 # targetMemoryUtilizationPercentage: 80
```

5. **Teste de Implementação**:
   - Inicie o `microk8s` se ainda não estiver rodando:
```
microk8s start
```

   - Crie um namespace:
```
microk8s.kubectl create namespace seu_namespace
```

   - Instale a aplicação usando o Helm3:
```
microk8s.helm3 install nome_da_release ./nome-do-meu-chart -n seu_namespace
```

6. **Verificação de Implementação**:
   Use os comandos abaixo para verificar sua implementação:
```
microk8s.helm3 list -n seu_namespace
microk8s.helm3 history -n seu_namespace nome_da_release
```

7. **Encaminhamento para Localhost**:
   Para acessar sua aplicação via localhost, execute o seguinte comando:
```
microk8s.kubectl port-forward nome_do_pod 8080:80 -n seu_namespace
```

   Agora, acesse http://localhost:8080 no navegador para ver sua aplicação rodando.

8. **Problemas no HPA**
   Caso enfrente problemas no HPA, certifique-se de utilizar a versão autoscaling/v2

9. **Versão depreciada no ingressClassName**
   Caso tenha este problema basta mudar no arquivo values.yaml, as linhas abaixo:
```
annotations:
   kubernetes.io/ingress.class: nginx
```
Para:
```
ingress:
   enabled: true
   ingressClassName: nginx
```


